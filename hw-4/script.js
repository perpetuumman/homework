        //Задание 1
        let ourArray = [1, 2, 3, 4, 5];
        document.write("Initial Array:" + ourArray + "</br>");
        function map(fn, array) {
            let testArray = [];
            for (i = 0; i < array.length; i++) {
                testArray[i] = fn(array[i]);
            };
            return testArray;
        }
        function double(number) {
            return number * 2;
        }
        document.write("New Array:" + map(double, ourArray));
        //Задание 2
        function checkAge(age) {
            return (age > 18) ? true : confirm('Родители разрешили?');
        }
        alert(checkAge(prompt("Введите ваш возраст:")));

