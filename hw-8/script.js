let counter = 0;

const createNewChild = () => {
    const item = document.getElementById("item");
    const itemNew = document.createElement("div");
    itemNew.innerHTML = "NEW ITEM";
    item.appendChild(itemNew);
    counter++;
    if (counter > 10) {
        item.remove();
    }
  
}